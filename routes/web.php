<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $locale;
    if (App::isLocale('en')) {
    	$locale = '/en';
    } else {
    	$locale = '/id';
    }
    return redirect($locale);
});

Route::group(['prefix' => '{locale}'], function() {
	Route::get('/', ['uses' => 'BaseController@home']);
	Route::get('about-us', ['uses' => 'BaseController@aboutUs']);
	Route::get('contact', ['uses' => 'BaseController@contact']);
});

Route::get('/lang/id', function() {
	App::setLocale('id');
	return redirect('/id');
});