@extends('layouts.master')

@section('page-title', __('contact.page-title'))

@push('external-scripts')
	<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyA9P-19NT0q9dIrTj5rfWYjGin9KiC6gsY"></script>
	<script type="text/javascript" src="{{asset('js/jquery.gmap.js')}}"></script>
	<script type="text/javascript">
		$('#google-map').gMap({

				address: 'Dayung V, Tangerang, Indonesia',
				maptype: 'ROADMAP',
				zoom: 14,
				markers: [
					{
						address: "Dayung V, Tangerang, Indonesia",
						html: '<div style="width: 330px;"><h4 style="margin-bottom: 8px;">Hi, we\'re <span>Envato</span></h4><p class="nobottommargin"><img src="{{asset('images/gallery/thumbs/1.jpg')}}" class="alignleft" alt="" style="margin: 5px 10px 1px 0;" />Our mission is to help people to <strong>earn</strong> and to <strong>learn</strong> online. We operate <strong>marketplaces</strong> where thousands of people buy and sell digital goods every day, and a network of educational blogs where millions learn <strong>creative skills</strong>.</p></div>',
						icon: {
							image: "{{asset('images/marker.png')}}",
							iconsize: [32, 39],
							iconanchor: [13,39]
						}
					}
				],
				doubleclickzoom: false,
				controls: {
					panControl: true,
					zoomControl: true,
					mapTypeControl: true,
					scaleControl: false,
					streetViewControl: false,
					overviewMapControl: false
				}
			});
	</script>
@endpush

@section('content')	
	<section id="google-map" class="gmap"></section>

	<!-- Content
	============================================= -->
	<section id="content">

		<div class="content-wrap">

			<div class="container clearfix">

				<!-- Postcontent
				============================================= -->
				<div class="postcontent nobottommargin">

					<h3>@lang('contact.send-email')</h3>

					<div class="contact-widget">

						<div class="contact-form-result"></div>

						<form class="nobottommargin" id="template-contactform" name="template-contactform" action="include/sendemail.php" method="post">

							<div class="form-process"></div>

							<div class="col_one_third">
								<label for="template-contactform-name">Name <small>*</small></label>
								<input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control required" />
							</div>

							<div class="col_one_third">
								<label for="template-contactform-email">Email <small>*</small></label>
								<input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control" />
							</div>

							<div class="col_one_third col_last">
								<label for="template-contactform-phone">Phone</label>
								<input type="text" id="template-contactform-phone" name="template-contactform-phone" value="" class="sm-form-control" />
							</div>

							<div class="clear"></div>

							<div class="col_two_third">
								<label for="template-contactform-subject">Subject <small>*</small></label>
								<input type="text" id="template-contactform-subject" name="template-contactform-subject" value="" class="required sm-form-control" />
							</div>

							<div class="col_one_third col_last">
								<label for="template-contactform-service">Services</label>
								<select id="template-contactform-service" name="template-contactform-service" class="sm-form-control">
									<option value="">-- Select One --</option>
									<option value="@lang('contact.maid')">@lang('contact.maid')</option>
									<option value="@lang('contact.cleaner')">@lang('contact.cleaner')</option>
									<option value="@lang('contact.labor')">@lang('contact.labor')</option>
								</select>
							</div>

							<div class="clear"></div>

							<div class="col_full">
								<label for="template-contactform-message">Message <small>*</small></label>
								<textarea class="required sm-form-control" id="template-contactform-message" name="template-contactform-message" rows="6" cols="30"></textarea>
							</div>

							<div class="col_full hidden">
								<input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
							</div>

							<div class="col_full">
								<button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Send Message</button>
							</div>

						</form>
					</div>

				</div><!-- .postcontent end -->

				<!-- Sidebar
				============================================= -->
				<div class="sidebar col_last nobottommargin">

					<div class="widget clearfix">

						<h3 class="nobottommargin uppercase">Jakarta</h3><br>

						<address>
							<strong>Location:</strong><br>
							Cyber Tower<br>
							Jakarta Selatan, 12810<br>
						</address>
						<abbr title="Phone Number"><strong>Phone:</strong></abbr> (91) 8547 632521<br>
						<abbr title="Fax"><strong>Fax:</strong></abbr> (91) 11 4752 1433<br>
						<abbr title="Email Address"><strong>Email:</strong></abbr> info@canvas.com

						<div class="line line-sm"></div>

						<h3 class="nobottommargin uppercase">Tangerang</h3><br>

						<address>
							<strong>Location:</strong><br>
							Jamsostek Tower<br>
							Jakarta Selatan, 13110<br>
						</address>
						<abbr title="Phone Number"><strong>Phone:</strong></abbr> (91) 8547 632521<br>
						<abbr title="Fax"><strong>Fax:</strong></abbr> (91) 11 4752 1433<br>
						<abbr title="Email Address"><strong>Email:</strong></abbr> info@canvas.com
					</div>

					<div class="line line-sm"></div>

					<div class="widget notopmargin clearfix">

						<a href="#" class="social-icon si-small si-dark si-facebook">
							<i class="icon-facebook"></i>
							<i class="icon-facebook"></i>
						</a>

						<a href="#" class="social-icon si-small si-dark si-twitter">
							<i class="icon-twitter"></i>
							<i class="icon-twitter"></i>
						</a>

						<a href="#" class="social-icon si-small si-dark si-dribbble">
							<i class="icon-dribbble"></i>
							<i class="icon-dribbble"></i>
						</a>

						<a href="#" class="social-icon si-small si-dark si-forrst">
							<i class="icon-forrst"></i>
							<i class="icon-forrst"></i>
						</a>

						<a href="#" class="social-icon si-small si-dark si-pinterest">
							<i class="icon-pinterest"></i>
							<i class="icon-pinterest"></i>
						</a>

						<a href="#" class="social-icon si-small si-dark si-gplus">
							<i class="icon-gplus"></i>
							<i class="icon-gplus"></i>
						</a>

					</div>

				</div><!-- .sidebar end -->

			</div>
			<a href="projects.html" class="button button-3d nobottomborder button-full center tright t300 font-primary topmargin footer-stick" style="font-size: 26px;">
				<div class="container clearfix">
					Would you like to Build your Dream Home with Us? <strong>Enquire Here</strong> <i class="icon-angle-right" style="top:3px;"></i>
				</div>
			</a>

		</div>

	</section><!-- #content end -->
@endsection
