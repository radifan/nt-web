@extends('layouts.master')

@section('page-title', __('home.page-title'))

@push('external-styles')
<style>
    .heading-block.center > span {
        max-width: 100% !important;
    }

    .slider-caption h2 {
        font-size: 40px !important;
    }

    .team-image-hover .overlay {
        opacity: 0;
        transition: .5s ease;
        height: 100%;
        width: 100%;
        background-color: rgba(0, 0, 0, 0.5);
    }

    .team-image-hover:hover .overlay {
        opacity: 1;
    }

    .overlay-text {
        color: white;
        font-size: 20px;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
    }
</style>
@endpush

@section('content')
	<section id="slider" class="slider-parallax force-full-screen full-screen">

		<div class="slider-parallax-inner">

			<div class="force-full-screen full-screen dark" style="background: url('images/kinara-01.jpg') center center;">

				<div class="container clearfix">
					<div class="slider-caption slider-caption-center">
						<h2 data-animate="fadeInDown">Selamat Datang di NueveTOP</h2>
						<p data-animate="fadeInUp" data-delay="400">Di tengah arus informasi yang deras, kami akan membawa brand Anda ke titik tengah kehidupan sehari-hari masyarakat.</p>
					</div>
				</div>

			</div>

		</div>

	</section>

	<!-- Page Sub Menu
	============================================= -->
	<div id="page-menu" class="dots-menu">

		<div id="page-menu-wrap">

			<div class="container clearfix">

				<nav class="one-page-menu no-offset">
					<ul>
						<li><a href="#" data-href="#header"><div>Home</div></a></li>
						<li><a href="#" data-href="#section-about"><div>About</div></a></li>
						<li><a href="#" data-href="#section-work"><div>Work</div></a></li>
						<li><a href="#" data-href="#section-services"><div>Services</div></a></li>
						<li><a href="#" data-href="#section-video"><div>Video</div></a></li>
						<li><a href="#" data-href="#section-testimonials"><div>Testimonials</div></a></li>
						<li><a href="#" data-href="#section-contact"><div>Contact</div></a></li>
					</ul>
				</nav>

			<div id="page-submenu-trigger"><i class="icon-reorder"></i></div>

			</div>

		</div>

	</div><!-- #page-menu end -->

	<!-- Content
	============================================= -->
	<section id="content">

		<div class="content-wrap nopadding">

			<section id="section-about" class="page-section section nobg nomargin">
				<div class="container clearfix">

					<div class="heading-block bottommargin-lg center">
						<h2>About Us.</h2>
						<span>NueveTOP adalah konsultan digital pengembangan brand. Kami akan membantu pemasaran produk brand Anda di ranah digital, melalui beragam cara. Kami dapat mendukung tampilan produk Anda dalam bentuk rancangan brand, logo, dan website baik untuk individu maupun perusahaan. Saat brand Anda sudah siap dan situs resminya telah aktif, kami akan memasarkannya ke media massa. Fokus kami adalah ranah digital, karena itu kami juga mampu membantu pemasaran melalui kanal Google Ads Network dan para Key Opinion Leader (KOL) atau influencer, sebelum perkenalan ke media. Mudah, Puas, dan Aman, karena kami dapat membantu Anda melalui online. Konsultasikan kebutuhan Anda!</span>
					</div>

					<div class="col_one_third">

						<div class="heading-block fancy-title nobottomborder title-bottom-border">
							<h4>WHY CHOOSING <span>US</span>.</h4>
						</div>

						<p>Keseharian kita kini tidak bisa lepas dari gadget dan ranah digital. Pada awal NueveTOP berdiri, kami hanya memposisikan diri sebagai vendor marketing event. Berkaca pada fakta bahwa percakapan dan kehidupan kita sehari-hari sudah terintegrasi dengan ranah digital, kami memutuskan untuk berkontribusi ke ranah ini sebagai konsultan digital pengembangan brand. NueveTOP berbasis di Jakarta, ibu kota Indonesia, titik pusat perputaran uang dan informasi. Kami terdiri atas individu-individu profesional di bidangnya masing-masing, yakni pemasaran digital, creative design, event production, design website (Website & Mobile), dan media specialist. </p>

					</div>

					<div class="col_one_third">

						<div class="heading-block fancy-title nobottomborder title-bottom-border">
							<h4>OUR <span>MISSION</span>.</h4>
						</div>

                        <p>Misi kami adalah memasarkan dan memperluas jangkauan Brand Anda. Kami akan membantu menyebarkan nilai-nilai yang diyakini Brand Anda kepada masyarakat. Kami akan merangkai cerita dan menggali nilai-nilai produk Anda.</p>

					</div>

					<div class="col_one_third col_last">

						<div class="heading-block fancy-title nobottomborder title-bottom-border">
							<h4>WHAT WE <span>DO</span>.</h4>
						</div>

						<p>Kami dapat merancang produk Anda dari nol. Selanjutnya, kami dapat merancang logo untuk brand Anda tersebut, kemudian membuat website brand untuk kehadirannya di ranah digital. Saat serangkaian identitas brand Anda sudah terbentuk, kami akan memasarkannya melalui media massa dan kanal digital. Berhubungan dengan media massa, kami akan menyarankan Anda untuk menggelar konferensi pers. Sementara, untuk kanal digital, mediumnya melalui pemasaran pada Google Ads Network dan online influencer pada kanal Instagram, Twitter, dan/atau Facebook.</p>

					</div>

					<div class="clear"></div>

				</div>
			</section>

      <section id="section-team" class="page-section section nomargin">
        <div class="heading-block center">
					<h2>Our Teams</h2>
				</div>

        <div class="container clearfix center">
					<div class="col-md-3 col-sm-6 bottommargin">

						<div class="team">
							<div class="team-image team-image-hover">
								<img src="images/team/1.jpg" alt="Debby Lufiasita">
                                <div class="overlay">
                                    <div class="overlay-text">
                                        Debby Lufiasita
                                    </div>
                                </div>
							</div>
							<div class="team-desc">
                                <a href="https://id.linkedin.com/in/debby-lufiasita" class="social-icon inline-block si-small si-light si-rounded si-linkedin">
                                    <i class="icon-linkedin"></i>
                                    <i class="icon-linkedin"></i>
                                </a>
							</div>
						</div>

					</div>

					<div class="col-md-3 col-sm-6 bottommargin">

						<div class="team">
							<div class="team-image team-image-hover">
								<img src="images/team/2.jpg" alt="Erika Anindita Dewi">
                                <div class="overlay">
                                    <div class="overlay-text">
                                        Erika Anindita Dewi
                                    </div>
                                </div>
							</div>
							<div class="team-desc">
                                <a href="https://fr.linkedin.com/in/eadewi" class="social-icon inline-block si-small si-light si-rounded si-linkedin">
                                    <i class="icon-linkedin"></i>
                                    <i class="icon-linkedin"></i>
                                </a>
							</div>
						</div>

					</div>

					<div class="col-md-3 col-sm-6 bottommargin">

						<div class="team">
							<div class="team-image team-image-hover">
								<img src="images/team/3.jpg" alt="Mato Sutrisno">
                                <div class="overlay">
                                    <div class="overlay-text">
                                        Mato Sutrisno
                                    </div>
                                </div>
							</div>
							<div class="team-desc">
                                <a href="#" class="social-icon inline-block si-small si-light si-rounded si-linkedin">
                                    <i class="icon-linkedin"></i>
                                    <i class="icon-linkedin"></i>
                                </a>
							</div>
						</div>

					</div>

					<div class="col-md-3 col-sm-6 bottommargin">

						<div class="team">
							<div class="team-image team-image-hover">
								<img src="images/team/4.jpg" alt="Basir S. Radifan">
                                <div class="overlay">
                                    <div class="overlay-text">
                                        Basir S. Radifan
                                    </div>
                                </div>
							</div>
							<div class="team-desc">
                                <a href="https://id.linkedin.com/in/basirsradifan" class="social-icon inline-block si-small si-light si-rounded si-linkedin">
                                    <i class="icon-linkedin"></i>
                                    <i class="icon-linkedin"></i>
                                </a>
							</div>
						</div>

					</div>

				</div>
      </section>

      <section id="section-services" class="page-section full-screen section dark nopadding nomargin noborder ohidden" style="background-color: #222;">
				<div class="vertical-middle">
					<div class="container clearfix">

						<div class="heading-block center bottommargin-lg">
							<h2>Services.</h2>
							<span>Sebagai konsultan digital pengembangan brand, kami menawarkan one-stop service yang meliputi asistensi pada creative design, website, digital marketing, dan media specialist.</span>
						</div>

						<div class="col_half">
							<div class="feature-box fbox-plain">
								<div class="fbox-icon">
									<a href="#"><i class="icon-screen"></i></a>
								</div>
								<h3>CREATIVE DESIGN</h3>
                                <p>Kami akan mengkomunikasikan nilai brand Anda dan merancang tampilan brand yang unik. Anda dapat menawarkan konsep grafis lalu mengonsultasikan alternatif tersebut kepada kami, dan kami akan menanggapinya dan memberi alternatif lain. Graphic Design, Brand Logo, Company Profile, Name Card, Banner Ads & Print, Brochure & Flyer, </p>
                            </div>
						</div>

						<div class="col_half col_last">
							<div class="feature-box fbox-plain">
								<div class="fbox-icon">
									<a href="#"><i class="icon-eye"></i></a>
								</div>
								<h3>WEBSITE DEVELOPMENT</h3>
                                <p>Kami merancang situs resmi brand Anda, yang akan menampilkan informasi-informasi berguna dan terbaru terkait produk perusahaan Anda. Individual, Corporate, E-Commerce, Mobile Apps Development, Domain & Hosting, </p>
                            </div>
						</div>

						<div class="col_half">
							<div class="feature-box fbox-plain">
								<div class="fbox-icon">
									<a href="#"><i class="icon-beaker"></i></a>
								</div>
								<h3>DIGITAL MARKETING</h3>
                                <p>Praktik pemasaran digital melalui kanal-kanal yang transparan dan efektif. Google Ads, Google AdWords, Google Display Networks, Online Advertising, </p>
                            </div>
						</div>

						<div class="col_half col_last">
							<div class="feature-box fbox-plain">
								<div class="fbox-icon">
									<a href="#"><i class="icon-stack"></i></a>
								</div>
								<h3>MEDIA SPECIALIST</h3>
                                <p>Kami menjadi penghubung dalam pemasaran produk Anda kepada media massa, melalui beragam bentuk. Product Launch, Developing Press Release, Printed Media Monitoring, Online Media Monitoring, </p>
                            </div>
						</div>

					</div>
				</div>
			</section>

			<section id="section-video" class="page-section full-screen section dark nopadding nomargin noborder ohidden">

				<div class="container vertical-middle center clearfix">
					<i class="i-plain i-xlarge icon-diamond divcenter bottommargin"></i>

					<div class="slider-caption slider-caption-center">
						<h2 data-animate="fadeInUp">Brilliant Service</h2>
						<p data-animate="fadeInUp" data-delay="200">"Momentum poverty tackling fellows social impact. Expanding community ownership, future affiliate protect civil society. Bloomberg."</p>
					</div>
				</div>
				<div class="video-wrap">
					<video poster="images/videos/explore.jpg" preload="auto" loop autoplay muted>
						<source src='images/videos/explore.mp4' type='video/mp4' />
						<source src='images/videos/explore.webm' type='video/webm' />
					</video>
					<div class="video-overlay" style="background-color: rgba(0,0,0,0.2);"></div>
				</div>

			</section>

			<section id="section-work" class="page-section section nomargin">

				<div class="heading-block center">
					<h2>Our Works</h2>
					<span>Some of the Awesome Projects we've worked on.</span>
				</div>

				<div class="container clearfix center">

					<!-- Portfolio Items
					============================================= -->
					<div id="portfolio" class="portfolio portfolio-nomargin clearfix">

						<article class="portfolio-item pf-media pf-icons">
							<div class="portfolio-image">
								<a href="portfolio-single.html">
									<img src="images/portfolio/4/1.jpg" alt="Open Imagination">
								</a>
								<div class="portfolio-overlay">
									<a href="images/portfolio/full/1.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
									<a href="portfolio-single.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
								</div>
							</div>
							<div class="portfolio-desc">
								<h3><a href="portfolio-single.html">Open Imagination</a></h3>
								<span><a href="#">Media</a>, <a href="#">Icons</a></span>
							</div>
						</article>

						<article class="portfolio-item pf-illustrations">
							<div class="portfolio-image">
								<a href="portfolio-single.html">
									<img src="images/portfolio/4/2.jpg" alt="Locked Steel Gate">
								</a>
								<div class="portfolio-overlay">
									<a href="images/portfolio/full/2.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
									<a href="portfolio-single.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
								</div>
							</div>
							<div class="portfolio-desc">
								<h3><a href="portfolio-single.html">Locked Steel Gate</a></h3>
								<span><a href="#">Illustrations</a></span>
							</div>
						</article>

						<article class="portfolio-item pf-graphics pf-uielements">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/portfolio/4/3.jpg" alt="Mac Sunglasses">
								</a>
								<div class="portfolio-overlay">
									<a href="http://vimeo.com/89396394" class="left-icon" data-lightbox="iframe"><i class="icon-line-play"></i></a>
									<a href="portfolio-single-video.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
								</div>
							</div>
							<div class="portfolio-desc">
								<h3><a href="portfolio-single-video.html">Mac Sunglasses</a></h3>
								<span><a href="#">Graphics</a>, <a href="#">UI Elements</a></span>
							</div>
						</article>

						<article class="portfolio-item pf-icons pf-illustrations">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/portfolio/4/4.jpg" alt="Mac Sunglasses">
								</a>
								<div class="portfolio-overlay" data-lightbox="gallery">
									<a href="images/portfolio/full/4.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
									<a href="images/portfolio/full/4-1.jpg" class="hidden" data-lightbox="gallery-item"></a>
									<a href="portfolio-single-gallery.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
								</div>
							</div>
							<div class="portfolio-desc">
								<h3><a href="portfolio-single-gallery.html">Morning Dew</a></h3>
								<span><a href="#"><a href="#">Icons</a>, <a href="#">Illustrations</a></span>
							</div>
						</article>

						<article class="portfolio-item pf-uielements pf-media">
							<div class="portfolio-image">
								<a href="portfolio-single.html">
									<img src="images/portfolio/4/5.jpg" alt="Console Activity">
								</a>
								<div class="portfolio-overlay">
									<a href="images/portfolio/full/5.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
									<a href="portfolio-single.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
								</div>
							</div>
							<div class="portfolio-desc">
								<h3><a href="portfolio-single.html">Console Activity</a></h3>
								<span><a href="#">UI Elements</a>, <a href="#">Media</a></span>
							</div>
						</article>

						<article class="portfolio-item pf-graphics pf-illustrations">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/portfolio/4/6.jpg" alt="Mac Sunglasses">
								</a>
								<div class="portfolio-overlay" data-lightbox="gallery">
									<a href="images/portfolio/full/6.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
									<a href="images/portfolio/full/6-1.jpg" class="hidden" data-lightbox="gallery-item"></a>
									<a href="images/portfolio/full/6-2.jpg" class="hidden" data-lightbox="gallery-item"></a>
									<a href="images/portfolio/full/6-3.jpg" class="hidden" data-lightbox="gallery-item"></a>
									<a href="portfolio-single-gallery.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
								</div>
							</div>
							<div class="portfolio-desc">
								<h3><a href="portfolio-single-gallery.html">Shake It!</a></h3>
								<span><a href="#">Illustrations</a>, <a href="#">Graphics</a></span>
							</div>
						</article>

						<article class="portfolio-item pf-uielements pf-icons">
							<div class="portfolio-image">
								<a href="portfolio-single-video.html">
									<img src="images/portfolio/4/7.jpg" alt="Backpack Contents">
								</a>
								<div class="portfolio-overlay">
									<a href="http://www.youtube.com/watch?v=kuceVNBTJio" class="left-icon" data-lightbox="iframe"><i class="icon-line-play"></i></a>
									<a href="portfolio-single-video.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
								</div>
							</div>
							<div class="portfolio-desc">
								<h3><a href="portfolio-single-video.html">Backpack Contents</a></h3>
								<span><a href="#">UI Elements</a>, <a href="#">Icons</a></span>
							</div>
						</article>

						<article class="portfolio-item pf-graphics">
							<div class="portfolio-image">
								<a href="portfolio-single.html">
									<img src="images/portfolio/4/8.jpg" alt="Sunset Bulb Glow">
								</a>
								<div class="portfolio-overlay">
									<a href="images/portfolio/full/8.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
									<a href="portfolio-single.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
								</div>
							</div>
							<div class="portfolio-desc">
								<h3><a href="portfolio-single.html">Sunset Bulb Glow</a></h3>
								<span><a href="#">Graphics</a></span>
							</div>
						</article>

						<article class="portfolio-item pf-illustrations pf-icons">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/portfolio/4/9.jpg" alt="Mac Sunglasses">
								</a>
								<div class="portfolio-overlay" data-lightbox="gallery">
									<a href="images/portfolio/full/9.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
									<a href="images/portfolio/full/9-1.jpg" class="hidden" data-lightbox="gallery-item"></a>
									<a href="images/portfolio/full/9-2.jpg" class="hidden" data-lightbox="gallery-item"></a>
									<a href="portfolio-single-gallery.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
								</div>
							</div>
							<div class="portfolio-desc">
								<h3><a href="portfolio-single.html">Bridge Side</a></h3>
								<span><a href="#">Illustrations</a>, <a href="#">Icons</a></span>
							</div>
						</article>

						<article class="portfolio-item pf-graphics pf-media pf-uielements">
							<div class="portfolio-image">
								<a href="portfolio-single-video.html">
									<img src="images/portfolio/4/10.jpg" alt="Study Table">
								</a>
								<div class="portfolio-overlay">
									<a href="http://vimeo.com/91973305" class="left-icon" data-lightbox="iframe"><i class="icon-line-play"></i></a>
									<a href="portfolio-single-video.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
								</div>
							</div>
							<div class="portfolio-desc">
								<h3><a href="portfolio-single-video.html">Study Table</a></h3>
								<span><a href="#">Graphics</a>, <a href="#">Media</a></span>
							</div>
						</article>

						<article class="portfolio-item pf-media pf-icons">
							<div class="portfolio-image">
								<a href="portfolio-single.html">
									<img src="images/portfolio/4/11.jpg" alt="Workspace Stuff">
								</a>
								<div class="portfolio-overlay">
									<a href="images/portfolio/full/11.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
									<a href="portfolio-single.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
								</div>
							</div>
							<div class="portfolio-desc">
								<h3><a href="portfolio-single.html">Workspace Stuff</a></h3>
								<span><a href="#">Media</a>, <a href="#">Icons</a></span>
							</div>
						</article>

						<article class="portfolio-item pf-illustrations pf-graphics">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/portfolio/4/12.jpg" alt="Mac Sunglasses">
								</a>
								<div class="portfolio-overlay" data-lightbox="gallery">
									<a href="images/portfolio/full/12.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
									<a href="images/portfolio/full/12-1.jpg" class="hidden" data-lightbox="gallery-item"></a>
									<a href="portfolio-single-gallery.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
								</div>
							</div>
							<div class="portfolio-desc">
								<h3><a href="portfolio-single-gallery.html">Fixed Aperture</a></h3>
								<span><a href="#">Illustrations</a>, <a href="#">Graphics</a></span>
							</div>
						</article>

					</div><!-- #portfolio end -->

				</div>

			</section>

      <section id="section-client" class="page-section section nobg nomargin">
        <div class="heading-block center">
					<h2>Our Clients</h2>
				</div>

        <div class="container clearfix center">
          <ul class="clients-grid grid-5 nobottommargin clearfix">
						<li><a href="http://www.warsteiner.com/our-beer/konig-ludwig-weisssbier/"><img src="images/clients/1.jpg" alt="Konig Ludwig Weissbier"></a></li>
						<li><a href="https://idn.taiwanexpoasean.com/"><img src="images/clients/3.jpg" alt="Taiwan Expo"></a></li>
						<li><a href="http://www.taitra.org.tw/"><img src="images/clients/4.jpg" alt="Taitra"></a></li>
                        <li><a href="http://nusantarun.com"><img src="images/clients/5.jpg" alt="NusantaRun"></a></li>
                        <li><a href="http://kinaraindonesia.com"><img src="images/clients/6.jpg" alt="Kinara Indonesia"></a></li>
					</ul>
        </div>
      </section>

      <section id="section-testimonials" class="page-section section parallax full-screen nomargin dark" style="background-image: url('images/kinara-02.jpg'); padding: 200px 0;" data-stellar-background-ratio="0.3">

				<div class="vertical-middle">

					<div class="container clearfix">

						<div class="col_half nobottommargin">&nbsp;</div>

						<div class="col_half nobottommargin col_last">

							<div class="heading-block center">
								<h4>What Clients say?</h4>
								<span>Some of our Clients love us &amp; so we do!</span>
							</div>

							<div class="fslider testimonial testimonial-full nobgcolor noborder noshadow nopadding" data-arrows="false">
								<div class="flexslider">
									<div class="slider-wrap">
										<div class="slide">
											<div class="testi-content">
												<p>The event ran well and the participants were satisfied with the results. It shows a good relationship and a good teamwork among the people who had given their best effort for the event.</p>
												<div class="testi-meta">
                                                    Moby Rachiem
													<span>CEO of SCUDS Kreatif Indo</span>
												</div>
											</div>
										</div>
										<div class="slide">
											<div class="testi-content">
												<p>At the beginning of my career, I didn't really think of having an official website. Thanks to NueveTop who has covered one of my online profile's presence very well, I can manage the relationship with my Lex Sugar through my website. We haven't yet made special contents, but we're currently working on it.</p>
												<div class="testi-meta">
                                                    Young Lex
													<span>Artist & CEO of YOGS Store</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>

					</div>

				</div>

			</section>

      <section id="map-overlay">
                <div class="container clearfix">

                    <!-- Contact Form Overlay
                    ============================================= -->
                    <div id="contact-form-overlay" class="clearfix">

                        <div class="fancy-title title-dotted-border">
                            <h3>Send us an Email</h3>
                        </div>

                        <div class="contact-widget">

                            <div class="contact-form-result"></div>

                            <!-- Contact Form
                            ============================================= -->
                            <form class="nobottommargin" id="template-contactform" name="template-contactform" action="include/sendemail.php" method="post">

                                <div class="col_half">
                                    <label for="template-contactform-service">Name <small>*</small></label>
                                    <input type="text" class="form-control" id="name" name="name">
                                </div>

                                <div class="col_half col_last">
                                    <label for="template-contactform-email">Email <small>*</small></label>
                                    <input type="email" class="form-control" id="email" name="email">
                                </div>

                                <div class="col_full">
                                    <label for="template-contactform-message">Message <small>*</small></label>
                                    <textarea class="required sm-form-control" id="template-contactform-message" name="template-contactform-message" rows="6" cols="30"></textarea>
                                </div>

                                <div class="col_full hidden">
                                    <input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
                                </div>

                                <div class="col_full">
                                    <button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Send Message</button>
                                </div>

                            </form>
                        </div>


                        <div class="line"></div>

                        <!-- Contact Info
                        ============================================= -->
                        <div class="col_full nobottommargin">

                            <address>
                                <strong>Headquarters:</strong><br>
                                +628 9999 11 889<br>
                                info@nuevetop.com<br>
                                Jl. Ciliman No.1A/B, Lantai 2 - Cikini, Jakarta Pusat
                            </address>

                        </div><!-- Contact Info End -->

                    </div><!-- Contact Form Overlay End -->

                </div>

                <!-- Google Map
                ============================================= -->
                <section id="google-map" class="gmap"></section>
			</section>

		</div>

	</section><!-- #content end -->
@endsection
