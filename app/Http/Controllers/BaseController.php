<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class BaseController extends Controller
{
    public function home($locale) {
        App::setLocale($locale);
        return view('home');
    }

    public function aboutUs($locale) {
		App::setLocale($locale);

		return view('about-us');    	
    }

    public function contact($locale) {
    	App::setLocale($locale);

    	return view('contact');
    }
}
